#!/bin/bash

# Clone the kernel-runs repo.
# Then, loop through all git repos/branches defined in `branches`
# and for each branch, find the sha of its HEAD using ls-remote
# and then update its branch in the kernel-runs repo, if needed.
#
# Also, write a .gitlab-ci.yml file into each branch into the kernel-runs
# repo, which simply includes the local branch-gitlab-ci.yml file here

# Do not mind the `cd dir; [...]; cd ..` recommendation:
# shellcheck disable=SC2103

set -ex

PRIV_GIT_REPO=git@gitlab.com:mrchapp/kernel-runs.git
PUB_GIT_REPO=https://gitlab.com/Linaro/lkft/kernel-runs.git

# Get the hash of any string
hash_string() {
  echo -n "$1" | sha1sum | cut -c1-40
}

# Read GL_CI_* variables from $1
# and write out .gitlab-ci.yml with that
write_gitlab_ci() {
  if [ -f "$1" ]; then
    eval "$(grep -E "^GL_CI_(REPO|BRANCH|FILE)=") $1"
  fi

  GL_CI_REPO="${GL_CI_REPO:-mrchapp/ci-scripts}"
  GL_CI_BRANCH="${GL_CI_BRANCH:-master}"
  GL_CI_FILE="${GL_CI_FILE:-branch-gitlab-ci.yml}"

  cat << EOF > .gitlab-ci.yml
include:
  - project: '${GL_CI_REPO}'
    ref: ${GL_CI_BRANCH}
    file: '${GL_CI_FILE}'
EOF
  unset GL_CI_REPO
  unset GL_CI_BRANCH
  unset GL_CI_FILE
}

# Parse branches file and commit changes locally
process_lines() {
  line="$*"
  echo "${line}"
  REPO_URL=$(echo "${line}" | awk '{print $1}')
  REPO_NAME=$(echo "${line}" | awk '{print $2}')
  BRANCH=$(echo "${line}" | awk '{print $3}')
  CONFIG=$(echo "${line}" | awk '{print $4}')
  GIT_BRANCH=${REPO_NAME}-${BRANCH}
  cd kernel-runs
  if git show-branch "origin/${GIT_BRANCH}"; then
    # branch exists
    git checkout "${GIT_BRANCH}"
  else
    # create a new empty branch
    git checkout --orphan "${GIT_BRANCH}"
    git rm -rf . || true # this fails on an empty repo
  fi
  REPO_HASH=$(hash_string "${REPO_URL}")
  if grep -q "refs/heads/${BRANCH}$" "${TMP_DIR}/${REPO_HASH}"; then
    LS_REMOTE=$(grep "refs/heads/${BRANCH}$" "${TMP_DIR}/${REPO_HASH}")
  else
    echo "WARNING: ${BRANCH} does not exist at ${REPO_URL}; ignoring" >&2
    return
  fi
  LATEST_SHA=$(echo "${LS_REMOTE}" | awk '{print $1}')
  echo "${LATEST_SHA}" > latest_sha
  write_gitlab_ci "${CONFIG}"
  echo "${REPO_URL}" > git_repo
  echo "${BRANCH}" > git_branch
  echo "${REPO_NAME}" > repo_name
  if [ -n "${CONFIG}" ]; then
    cp -p "${TOP_DIR}/configs/${CONFIG}" pipeline_config
  else
    rm -f pipeline_config
  fi
  if ! git status | grep -q "nothing to commit"; then
    git add --all .
    git commit -m "${REPO_NAME} ${BRANCH} ${LATEST_SHA}"
    if [ -z "${DRYRUN}" ]; then
      git push origin "${GIT_BRANCH}"
    fi
  fi
}

if [ -n "${CI}" ]; then
  [ -d ~/.ssh ] || mkdir -p ~/.ssh
  ssh-keyscan gitlab.com > ~/.ssh/known_hosts
fi

if [ -z "${DRYRUN}" ]; then
  git clone "${PRIV_GIT_REPO}"
else
  git clone "${PUB_GIT_REPO}"
fi

TMP_DIR=$(mktemp -d)

# Save all remote heads of all Git repos
awk '{ print $1 }' branches branches-dev | sort -u | while read -r REPO_URL; do
  REPO_HASH=$(hash_string "${REPO_URL}")
  git ls-remote --heads "${REPO_URL}" > "${TMP_DIR}/${REPO_HASH}"
done

TOP_DIR="$(pwd)"
while IFS= read -r line; do
  cd "${TOP_DIR}"
  process_lines "$line"
done < "${TOP_DIR}/branches"

if [ -f "${TOP_DIR}/branches-dev" ]; then
  while IFS= read -r line; do
    cd "${TOP_DIR}"
    process_lines "$line"
  done < "${TOP_DIR}/branches-dev"
fi

rm -rf "${TMP_DIR}"
